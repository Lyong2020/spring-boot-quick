package com.quick.druid.sys.mapper;

import com.quick.druid.sys.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author vector4wang
 * @since 2019-12-19
 */
public interface UserMapper extends BaseMapper<User> {

}
